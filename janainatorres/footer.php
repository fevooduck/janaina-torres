<footer class="footer">
	<div class="container">
        <div class="row">
        	<div class="col-md-3 col-md-offset-1">
            	<p>Rua Joaquim Antunes 177, sala 11<br>
				São Paulo, Brasil  05415-010<br>
				Tel +55 11 2367.9195</p>
            </div>
        	<div class="col-md-3 col-md-offset-1">
            	<p>Seg a Sexta 10h-19h Sab 11h-15h<br>
				<a href="mailto:contato@janainatorres.com.br">contato@janainatorres.com.br</a></p>
            </div>
        	<div class="col-md-3 col-md-offset-1">
            	<p><a href="<?php echo home_url(); ?>/privacidade-termos">Privacidade &amp; Termos</a><br>
				© <?php date('Y'); ?> Todos os Direitos Reservados</p>
            </div>
        </div>
    </div>
</footer>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js"></script>
                   <script type="text/javascript">
     jQuery(document).ready(function() {
	 var highestCol = Math.max(jQuery('.info-obras').height(),jQuery('.btn-box-obras').height()); // Pega altura da Div main
	 jQuery('.info-obras').height(highestCol); //Aplica altura da div main à div sidebar
	 jQuery('.btn-box-obras').height(highestCol); //Aplica altura da div main à div sidebar
     });
</script>

    <?php wp_footer(); ?>
  </body>
</html>