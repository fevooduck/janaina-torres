<?php get_header(); ?>
		<?php if (have_posts()) :
			while (have_posts()) : the_post(); 
			$titulo = get_the_title();
		?>
<section class="internas">
	<div class="container">
        <div class="row">
        	<div class="col-md-7 col-md-offset-1">
        	<h1 class="titulo-post"><?php echo $titulo; ?></h1>
        	<p><?php echo date('j \d\e F \d\e Y'); ?> • <?php the_category(', ') ?></p>
			<?php the_content("Leia Mais"); ?>
			<?php endwhile; endif; ?>
			<hr>
			<?php

				// $idPost = get_the_ID();
				// $args = array(
				// 'posts_per_page' => 5,
				// 'post__not_in' => array($idPost),
				// 'cat' =>  "-39, -3, -21, -11, -25, -7,-8,-19, -20, -1",
				// );
				$catidlist = '';
				$categories = get_the_category();
foreach( $categories as $category) {
    $catidlist .= $category->cat_ID . ",";
}

$catidlistArray = explode(",", $catidlist);
$catidexclude = array(39, 3, 21, 11, 25, 7, 8, 19, 20, 1);
$catid = array_diff($catidlistArray,$catidexclude);
$catid2 = implode(",", $catid);

$custom_query_args = array( 
	'posts_per_page' => 5, 
	'post__not_in' => array($post->ID), 
	'orderby' => 'date', 
	'order' => 'DESC', 
	'cat' => $catid2, 
);
// Initiate the custom query
$the_query = new WP_Query( $custom_query_args );
				// $the_query = new WP_Query( $args ); 
				echo '<section>'; 
				echo '<h4 class="titulo-veja">Veja também</h4>';
				echo '<ul class="list-unstyled">';
				while ( $the_query->have_posts() ) : $the_query->the_post();
				?> 
				<li>
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</li>
				<?php
				endwhile; 
				echo '</ul>'; 
				echo '</section>'; 
				wp_reset_postdata(); 
			?>			
			
			
			
			</div>

            	<?php get_sidebar(); ?>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
