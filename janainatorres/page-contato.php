<?php get_header(); 

/* Template Name: Contato */

?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<section class="internas page">
	<div class="container">
        <div class="row">
        	<div class="col-md-8 col-md-offset-2">
            	<?php the_content('Read the rest of this entry &raquo;'); ?>
            	
<form method="post" action="<?php echo home_url(); ?>/contato-enviado-com-sucesso/">
  <div class="form-group">
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Seu nome" name="nome" required>
  </div>
  <div class="form-group">
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Seu email" name="email" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Seu telefone" name="telefone">
  </div>
  <div class="form-group">
    <textarea class="form-control" rows="5" id="mensagem" placeholder="Deixe uma mensagem" name="mensagem"></textarea>
  </div>
  <p class="text-right"><button type="submit" class="btn-jtorres">Enviar</button></p>
</form>
            	
            </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</section>
  <?php get_footer(); ?>