<?php get_header(); ?>

<section class="internas">

	<div class="container">

        <div class="row">

        	<div class="col-md-7 col-md-offset-1">
        	
        	<?php 
             if (is_category('3')) {
            echo do_shortcode('[latest_stickies]'); 
                }
            ?>

        	<?php $the_query = new WP_Query( array( 'post__not_in' => get_option( 'sticky_posts' ) ) );
if ( $the_query->have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post();
                //if (have_posts()) :
                  //while (have_posts()) : the_post();
			  $titulo = get_the_title();

			  $permalink = get_the_permalink();

		?>

 				<article <?php post_class(); ?>>

 				<div class="row margin-exposicao">

				  <div class="col-md-12">

					  	<h1 class="titulo-post"><a href="<?php echo $permalink; ?>"><?php echo $titulo; ?></a></h1>

						<p><?php echo the_time('F j, Y'); ?> | <?php the_category(', ') ?></p>

						<?php the_content("Leia Mais"); ?>

				   </div>

            </div>

            </article>

            <?php endwhile; endif; ?>

        </div>

        <?php get_sidebar(); ?>

    </div>

    </div>

</section>

  <?php get_footer(); ?>

  

