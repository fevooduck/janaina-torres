<?php get_header(); ?>
		<?php if (have_posts()) :
			while (have_posts()) : the_post(); 
			$html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
			$titulo = get_the_title();
			$permalink = get_the_permalink();
			$artista = get_post_meta( $post->ID, 'obras_artista', true );
			$serie = get_post_meta( $post->ID, 'obras_series', true );
			$tecnica = get_post_meta( $post->ID, '_obras_tecnica', true );
			$dimensao1 = get_post_meta( $post->ID, '_obras_dimensao1', true );
			$dimensao2 = get_post_meta( $post->ID, '_obras_dimensao2', true );
			$dimensao3 = get_post_meta( $post->ID, '_obras_dimensao3', true );
			$video = get_post_meta( $post->ID, '_obras_video', true );
  			$ano = get_post_meta( $post->ID, '_obras_ano', true );
			$nome_artista = get_post( $artista ); 
			$titulo_artista = $nome_artista->post_title;
			if($serie != -1){
			$nome_serie = get_post( $serie ); 
			$titulo_serie = $nome_serie->post_title;
			}else{
				$titulo_serie = -1;
			}
		?>
<section class="internas">
	<div class="container">
        <div class="row">
        	<div class="col-md-8 col-md-offset-2">
				<?php if($video == "sim"){ 
					the_content();
				?>
				<div class="row box-obras">
				   <div class="col-md-10 info-obras">
						<h3 class="titulo-artista-archive"><?php echo $titulo_artista; ?></h3>
				    	<h2 class="titulo-obra-exposicao"><?php echo $titulo; ?></h2>
					    <p class="texto-descricao"><em><?php echo $tecnica; ?></em></p>
				   </div>
                </div>
				<?php }else { ?>
				<img src="<?php echo $html[0]; ?>" alt="<?php echo $titulo; ?>" class="img-responsive">
				<div class="row box-obras">
				   <div class="col-md-10 info-obras">
						<h3 class="titulo-artista-archive"><?php echo $titulo_artista; ?></h3>
				    	<h2 class="titulo-obra-exposicao"><?php echo $titulo; ?><?php if($ano !=''){ echo ", ".$ano;} ?></h2>
					    <p class="texto-descricao"><em><?php echo $tecnica; ?></em></p>
						<p class="texto-descricao"><?php echo $dimensao1; ?></p>
						<?php if($dimensao2 !=""){ ?>
						<p class="texto-descricao"><?php echo $dimensao2; ?></p>
						<?php } if($dimensao3 !=""){ ?>
						<p class="texto-descricao"><?php echo $dimensao3; ?></p>
						<?php } ?>
				   </div>
           		   <div class="col-md-2 btn-box-obras">
						<form action="<?php echo home_url(); ?>/mais-informacoes/" method="post">
							<input type="hidden" value="<?php echo $titulo; ?>" name="obra">
							<input type="hidden" value="<?php echo $tecnica; ?>" name="tecnica">
							<input type="hidden" value="<?php echo $titulo_artista; ?>" name="artista">
							<input type="hidden" value="<?php echo $dimensao1; ?>" name="dimensao1">
							<input type="hidden" value="<?php echo $dimensao2; ?>" name="dimensao2">
							<input type="hidden" value="<?php echo $dimensao3; ?>" name="dimensao3">
							<input type="hidden" value="<?php echo $titulo_serie; ?>" name="serie">
							<button type="submit" class="btn-jtorres">consulte</button>
						</form>
           		   </div>
                </div>
                <br><br>
				<?php 
					the_content();
					}
				endwhile; endif; ?>
 			</div>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
