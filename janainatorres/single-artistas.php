<?php get_header(); ?>
<section class="internas">
	<div class="container">
        <div class="row">
        	<div class="col-md-10 col-md-offset-1">
		<?php if (have_posts()) :
			while (have_posts()) : the_post(); 
			$html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
			$id = $post->ID;
			$nomeartista = get_the_title();
			$link = get_post_meta( $post->ID, 'link_curriculo', true );
		?>
				<div class="row margin-box-artistas">
				   <div class="col-md-4 box-artistas">
						<h2 class="titulo-artista"><?php echo $nomeartista; ?></h2>
						<?php the_excerpt(); ?>
						<a href="<?php echo $link; ?>" class="btn-box-artista btn-jtorres" target="_blank">veja currículo</a>
				   </div>
           		   <div class="col-md-8 foto-artistas">
						<img src="<?php echo $html[0]; ?>" alt="<?php echo $nomeartista; ?>" class="img-responsive">
           		   </div>
            	<?php endwhile; endif; ?>
                </div>
                <div class="row">
    <?php $cont=0;?>
    <?php $wp_query = new WP_Query(array( 
          'post_type' 		=> array('obras', 'series'),
          'orderby'           => 'post_date',
          'order' 		    => 'DESC',
          'posts_per_page'    => -1,
		  'meta_query' => array(
			array(
			'relation' => 'OR',
                        /*array(
                                'key' => 'obras_artista',
                                'value' => $id
                        ),*/
	 array(
                        'relation' => 'AND',
                        array(
                                'key' => 'obras_artista',
                                'value' => $id
                        ),
                        array(
                                'key' => 'obras_series',
                                'value' => -1,
                                'compare' => '==',
                        ),
						array(
                                'key' => '_obras_exclusivo',
                                'value' => "nao",
                                'compare' => '==',
                        ),
		),
                        array(
                                'key' => 'series_artista',
                                'value' => $id
                        ),
		),

)
		  	));
  while ( have_posts() ) : the_post();
  $cont++; 
			$titulo = get_the_title();
			$permalink = get_the_permalink();
			$obra_serie = get_post_meta( $post->ID, 'obras_series', true );
			if($obra_serie == -1){
			$thumb = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'thumb-galeria');
			$textoSerie = "";
			$ano = get_post_meta( $post->ID, '_obras_ano', true );
			} else{
			$thumb = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'thumb-galeria-serie');
			$textoSerie = "Série: ";
			$ano = get_post_meta( $post->ID, '_series_ano', true );
			}
			$tecnica = get_post_meta( $post->ID, '_obras_tecnica', true );
			$dimensao1 = get_post_meta( $post->ID, '_obras_dimensao1', true );
			$dimensao2 = get_post_meta( $post->ID, '_obras_dimensao2', true );
			$dimensao3 = get_post_meta( $post->ID, '_obras_dimensao3', true );
   ?>
            <div class="col-md-6 margin-obras-artistas">
          		<a href="<?php the_permalink(); ?>" title="<?php echo $titulo; ?>"><img src="<?php echo $thumb; ?>" alt="<?php echo $titulo; ?>" class="img-responsive"></a>
				<h3 class="titulo-artista-archive"><?php echo $nomeartista; ?></h3>
				<h2 class="titulo-obra-exposicao"><?php echo $textoSerie.$titulo; ?><?php if($ano !=''){ echo ", ".$ano;} ?></h2>
				<p class="texto-descricao"><em><?php echo $tecnica; ?></em></p>
				<p class="texto-descricao"><?php echo $dimensao1; ?></p>
				<?php if($dimensao2 !=""){ ?>
				<p class="texto-descricao"><?php echo $dimensao2; ?></p>
				<?php } if($dimensao3 !=""){ ?>
				<p class="texto-descricao"><?php echo $dimensao3; ?></p>
				<?php } ?>
           </div>
                  <?php
			if($cont == 2){
	  echo '</div>
	  <div class="row">';
				$cont = 0;
  };
                    endwhile; ?>
         	
                </div>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
