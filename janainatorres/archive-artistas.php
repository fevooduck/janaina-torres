<?php get_header(); ?>
<section class="internas">
	<div class="container">
        <div class="row">
        	<div class="col-md-10 col-md-offset-1">
				<div class="row">
		<?php if (have_posts()) :
			$cont = 0;
			while (have_posts()) : the_post(); 
			$cont++;
			$html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
		?>
           <div class="col-md-4 margin-artistas">
          		<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo $html[0]; ?>" alt="<?php the_title(); ?>" class="img-responsive"></a>
           		<h2 class="titulo-artista-archive"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
           </div>
            <?php
			if($cont == 3){
	  echo '</div>
	  <div class="row">';
				$cont = 0;
  };
                    endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
