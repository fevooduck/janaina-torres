<?php get_header(); ?>
		<?php if (have_posts()) :
			while (have_posts()) : the_post(); 
			$titulo = get_the_title();
			$artista = get_post_meta( $post->ID, 'series_artista', true );
			$nome_artista = get_post( $artista ); 
			$titulo_artista = $nome_artista->post_title;
			$id = $post->ID;
		?>
<section class="cab-page">
	<div class="container">
    	<div class="page-header">
        	<div class="row">
            	<div class="col-md-12">
                	<h1 class="text-center"><?php echo $titulo; ?></h1>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="internas">
	<div class="container">
        <div class="row">
        	<div class="col-md-8 col-md-offset-2">
        	<?php the_content(); ?>
<?php endwhile; endif; ?>
    <?php $wp_query = new WP_Query(array( 
          'post_type' 		=> 'obras',
          'orderby'           => 'menu_order',
          'order' 		    => 'ASC',
          'posts_per_page'    => -1,
		  'meta_key' => 'obras_series',
          'meta_value' => $id
		  	));
  			while ( have_posts() ) : the_post();
			
        	$html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
			$permalink = get_the_permalink();
			$titulo_obra = get_the_title();
			$tecnica = get_post_meta( $post->ID, '_obras_tecnica', true );
			$dimensao1 = get_post_meta( $post->ID, '_obras_dimensao1', true );
			$dimensao2 = get_post_meta( $post->ID, '_obras_dimensao2', true );
			$dimensao3 = get_post_meta( $post->ID, '_obras_dimensao3', true );
			$ano = get_post_meta( $post->ID, '_obras_ano', true );
			?>
				<img src="<?php echo $html[0]; ?>" alt="<?php echo $titulo_obra; ?>" class="img-responsive">
				<div class="row box-obras margin-obras-series">
				   <div class="col-md-10 info-obras">
						<h3 class="titulo-artista-archive"><?php echo $titulo_artista; ?></h3>
				    	<h2 class="titulo-obra-exposicao"><?php echo $titulo_obra; ?><?php if($ano !=''){ echo ", ".$ano;} ?></h2>
					    <p class="texto-descricao"><em><?php echo $tecnica; ?></em></p>
						<p class="texto-descricao"><?php echo $dimensao1; ?></p>
						<?php if($dimensao2 !=""){ ?>
						<p class="texto-descricao"><?php echo $dimensao2; ?></p>
						<?php } if($dimensao3 !=""){ ?>
						<p class="texto-descricao"><?php echo $dimensao3; ?></p>
						<?php } ?>
				   </div>
           		   <div class="col-md-2 btn-box-obras">
						<form action="<?php echo home_url(); ?>/mais-informacoes/" method="post">
							<input type="hidden" value="<?php echo $titulo_obra; ?>" name="obra">
							<input type="hidden" value="<?php echo $tecnica; ?>" name="tecnica">
							<input type="hidden" value="<?php echo $titulo_artista; ?>" name="artista">
							<input type="hidden" value="<?php echo $dimensao1; ?>" name="dimensao1">
							<input type="hidden" value="<?php echo $dimensao2; ?>" name="dimensao2">
							<input type="hidden" value="<?php echo $dimensao3; ?>" name="dimensao3">
							<input type="hidden" value="<?php echo $titulo; ?>" name="serie">
							<button type="submit" class="btn-jtorres">consulte</button>
						</form>
           		   </div>
                </div>
				   <?php  endwhile; ?>                
			</div>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
