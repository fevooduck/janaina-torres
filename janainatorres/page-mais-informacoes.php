<?php get_header(); 

/* Template Name: Mais Informações */

$obra = $_POST['obra'];
$tecnica = $_POST['tecnica'];
$dimensao1 = $_POST['dimensao1'];
$dimensao2 = $_POST['dimensao2'];
$dimensao3 = $_POST['dimensao3'];
$artista = $_POST['artista'];
$serie =  $_POST['serie'];


?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<section class="cab-page">
	<div class="container">
    	<div class="page-header">
        	<div class="row">
            	<div class="col-md-12">
                	<h1 class="text-center"><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
	</div>
</section>
<section class="internas page">
	<div class="container">
        <div class="row">
        	<div class="col-md-8 col-md-offset-2">
            	<form method="post" action="<?php echo home_url(); ?>/enviado-com-sucesso/">
  <div class="form-group">
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Seu nome" name="nome" required>
  </div>
  <div class="form-group">
    <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Seu email" name="email" required>
  </div>
  <div class="form-group">
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Seu telefone" name="telefone">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Obra</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="<?php echo $obra ?>" disabled>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Artista</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="<?php echo $artista ?>" disabled>
  </div>
    <?php if($serie != -1){?>
  <div class="form-group">
    <label for="exampleInputEmail1">Série</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="<?php echo $serie ?>" disabled>
  </div>
    <?php } ?>
  <div class="form-group">
    <label for="exampleInputEmail1">Técnica</label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="<?php echo $tecnica ?>" disabled>
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1"><?php if($dimensao2 !=""){?>Dimensões<?php }else{ ?>Dimensão<?php }?></label>
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="<?php echo $dimensao1 ?> <?php if($dimensao2 !=''){?>|<?php } ?> <?php echo $dimensao2 ?><?php if($dimensao3 !=''){?>|<?php } ?> <?php echo $dimensao3 ?>" disabled>
  </div>
  <div class="form-group">
  	<label for="comentarios">Comentários Adicionais</label>
    <textarea class="form-control" rows="5" placeholder="Deixe um comentário" name="adicionais"></textarea>
  </div>
  <p class="text-right"><button type="submit" class="btn-jtorres">Enviar</button></p>
    <input type="hidden" name="obra" value="<?php echo $obra ?>">
     <input type="hidden" name="artista" value="<?php echo $artista ?>">
     <input type="hidden" name="serie" value="<?php echo $serie ?>">
     <input type="hidden" name="tecnica" value="<?php echo $tecnica ?>">
     <input type="hidden" name="dimensao" value="<?php echo $dimensao1 ?> <?php if($dimensao2 !=''){?>|<?php } ?> <?php echo $dimensao2 ?><?php if($dimensao3 !=''){?>|<?php } ?> <?php echo $dimensao3 ?>">
</form>

            </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
