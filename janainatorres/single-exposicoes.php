<?php get_header(); ?>
		<?php if (have_posts()) :
			while (have_posts()) : the_post(); 
			  $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
			  $titulo = get_the_title();
			  $caixa = get_post_meta( $post->ID, '_exposicoes_caixa', true );
			  $estiloT = get_post_meta( $post->ID, '_exposicoes_estiloT', true );
			  $corT = get_post_meta( $post->ID, '_exposicoes_corT', true );
			  $subtitulo = get_post_meta( $post->ID, '_exposicoes_subtitulo', true );
			  $estiloS = get_post_meta( $post->ID, '_exposicoes_estiloS', true );
			  $corS = get_post_meta( $post->ID, '_exposicoes_corS', true );
			  $data = get_post_meta( $post->ID, '_exposicoes_data', true );
			  $estiloD = get_post_meta( $post->ID, '_exposicoes_estiloD', true );  
			  $corD = get_post_meta( $post->ID, '_exposicoes_corD', true );
			  $vista1 = get_post_meta( $post->ID, '_exposicoes_vista1', true );
			  $vista2 = get_post_meta( $post->ID, '_exposicoes_vista2', true );
			  $vista3 = get_post_meta( $post->ID, '_exposicoes_vista3', true );
			  $vista4 = get_post_meta( $post->ID, '_exposicoes_vista4', true );
			  $idExposicao = $post->ID;

			if($estiloT == "bold"){
				$estiloTitle = "exposicoes-bold";
			}elseif($estiloT == "regular"){
				$estiloTitle = "exposicoes-regular";
			}elseif($estiloT == "light"){
				$estiloTitle = "exposicoes-light";
			};

			if($estiloS == "bold"){
				$estiloSub = "exposicoes-bold";
			}elseif($estiloS == "regular"){
				$estiloSub = "exposicoes-regular";
			}elseif($estiloS == "light"){
				$estiloSub = "exposicoes-light";
			};

			if($estiloD == "bold"){
				$estiloDate = "exposicoes-bold";
			}elseif($estiloD == "regular"){
				$estiloDate = "exposicoes-regular";
			}elseif($estiloD == "light"){
				$estiloDate = "exposicoes-light";
			};

endwhile; endif; 

    $query = new WP_Query(array( 
          'post_type' 		=> 'vistas',
          'showposts'    => -1,
          'meta_query' => array(
            array(
                'key' => 'nome_exposicao',
                'value' => '"' .$idExposicao . '"',
                'compare' => 'like'
                )
            )
  ));

  if ($query->posts) {
  $i=0;
  while ( $query->have_posts() ) : $query->the_post();

  $imagens_vistaH = get_field('imagens_vista');

  $vistaID = get_the_ID();
  $vista[$vistaID]['id'] = $vistaID;
  $vista[$vistaID]['titulo'] = get_the_title();
  $imagens[$vistaID] = get_field('imagens_vista', $vistaID);

  $i++;

  endwhile; 
}

$h = 0;
foreach($imagens as $key => $linha){
	
	for($z=0;$z<count($linha);$z++){
	
		$novo[$h]['titulo'] = $vista[$key]['titulo'];
		$novo[$h]['posicaoV'] = $imagens[$key][$z]['posicao'];
		$novo[$h]['foto_grande'] = $imagens[$key][$z]['foto_grande'];
		$novo[$h]['foto_mobile'] = $imagens[$key][$z]['foto_mobile'];
		
		$h++;
	}
	
	
}	

$order = array();


// populate order
foreach( $novo as $i => $row ) {
	
	$order[ $i ] = $row['titulo'];

}

// multisort
array_multisort( $order, SORT_ASC, $novo );


   ?>
<section class="internas">
	<div class="container">
        <div class="row">
        	<div class="col-md-10 col-md-offset-1">

  <div id="carousel-slide-vistainicial" class="carousel slide carousel-fade" data-ride="carousel">
  
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
		<?php
		$h = 0;
		$posicao = 0;
		foreach($imagens_vistaH as $vistaH){
			$h++;
				if(in_array($posicao, $vistaH['posicao'])){
					if($h==1){
						echo '<div class="item active">';
					} else{
						echo '<div class="item">';
					}
					echo '<img src="'.$vistaH['foto_grande'].'" alt="'.$titulo.'" class="img-responsive hidden-xs">';
					echo '<img src="'.$vistaH['foto_mobile'].'" alt="'.$titulo.'" class="img-responsive visible-xs">';
					echo '</div>';
				}
		

		}

  ?>
  </div> 
    <? if ($h >1){ ?>
  <a class="left carousel-control-exposicao" href="#carousel-slide-vistainicial" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control-exposicao" href="#carousel-slide-vistainicial" role="button" data-slide="next">
    <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <?php } ?>

  </div>
				<div class="row box-exposicao">
				   <div class="col-md-7 info-exposicao">
						<h2 style="color:<?php echo $corT; ?>" class="titulo-exposicoes <?php if($caixa == 'sim'){?>text-uppercase<?php } ?> <?php echo $estiloTitle; ?>"><?php echo $titulo; ?></h2>
						<h3 style="color:<?php echo $corS; ?>" class="subtitulo-exposicoes <?php echo $estiloSub; ?>"><?php echo $subtitulo; ?></h3>
				   </div>
           		   <div class="col-md-5">
						<h3 class="header-artista-exposicao">Data</h3>
          		   		<p style="color:<?php echo $corD; ?>" class="data-exposicoes <?php echo $estiloDate; ?>"><?php echo $data; ?></p>
           		   </div>
            	
                </div>
                <div class="row">
    <?php $cont=0;?>
    <?php $obras_query = new WP_Query(array( 
          'post_type' 		=> 'obras',
          'orderby'           => 'post_date',
          'order' 		    => 'ASC',
          'posts_per_page'    => -1,
		  'meta_key' => 'obras_exposicao',
          'meta_value' => $idExposicao
		  	));
  while ( $obras_query->have_posts() ) : $obras_query->the_post();
  $cont++;
			$thumb = MultiPostThumbnails::get_post_thumbnail_url(get_post_type(), 'thumb-galeria');
			$titulo = get_the_title();
			$permalink = get_the_permalink();
			$artista = get_post_meta( $post->ID, 'obras_artista', true );
			$tecnica = get_post_meta( $post->ID, '_obras_tecnica', true );
			$dimensao1 = get_post_meta( $post->ID, '_obras_dimensao1', true );
			$dimensao2 = get_post_meta( $post->ID, '_obras_dimensao2', true );
			$dimensao3 = get_post_meta( $post->ID, '_obras_dimensao3', true );
			$nome_artista = get_post( $artista ); 
			$titulo_artista = $nome_artista->post_title;
  			$ano = get_post_meta( $post->ID, '_obras_ano', true );
  			$contador = $cont;
 ?>
            <div class="col-md-6 margin-exposicao">
          		<a href="<?php the_permalink(); ?>" title="<?php echo $titulo; ?>"><img src="<?php echo $thumb; ?>" alt="<?php echo $titulo; ?>" class="img-responsive"></a>
				<h3 class="titulo-artista-archive"><?php echo $titulo_artista; ?></h3>
				<h2 class="titulo-obra-exposicao"><?php echo $titulo; ?><?php if($ano !=''){ echo ", ".$ano;} ?></h2>
				<p class="texto-descricao"><em><?php echo $tecnica; ?></em></p>
				<p class="texto-descricao"><?php echo $dimensao1; ?></p>
				<?php if($dimensao2 !=""){ ?>
				<p class="texto-descricao"><?php echo $dimensao2; ?></p>
				<?php } if($dimensao3 !=""){ ?>
				<p class="texto-descricao"><?php echo $dimensao3; ?></p>
				<?php } ?>
           </div>
                  <?php
			/*if($cont == 2){
	  echo '</div>
	  <div class="row">';
				$cont = 0;
  };*/
  if(($cont % 2) == 0)
{
	  echo '</div>';
	  //echo '$cont = '.$cont;
	  //echo '<br>';
?>
  <div id="carousel-slide-vista<?php echo $cont;?>" class="carousel slide carousel-fade margin-exposicao" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
		<?php
		$a= 0;
		$posicao = $cont;
foreach( $novo as $i => $row ): 
if( in_array($posicao, $row['posicaoV'])){
$a++;
	if($a==1){
		echo '<div class="item active">';
	} else{
		echo '<div class="item">';
	}
	echo '<img src="'.$row['foto_grande'].'" alt="'.$titulo.'" class="img-responsive hidden-xs">';
	echo '<img src="'.$row['foto_mobile'].'" alt="'.$titulo.'" class="img-responsive visible-xs">';
	echo '</div>';
}

endforeach; 
?>
  </div> 
  <? if ($a >1){ ?>
  <!-- Controls -->
  <a class="left carousel-control-exposicao" href="#carousel-slide-vista<?php echo $cont;?>" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control-exposicao" href="#carousel-slide-vista<?php echo $cont;?>" role="button" data-slide="next">
    <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a> 
  <?php } ?>
  </div>
<?php

	  echo '<div class="row">';
}
                    endwhile; ?>
         	
                </div>

                
                
			</div>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
