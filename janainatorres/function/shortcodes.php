<?php

//////////////////////////////////////////////////////////////////
// Coluna shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('colunas', 'shortcode_colunas');
	function shortcode_colunas($atts, $content = null) {
		extract(shortcode_atts(array(
        'qtde'      => '#',
    ), $atts));
	$out .= '<div class="col-md-' . $qtde . '';
	$out .= '">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	
   return $out;

	}
	
//////////////////////////////////////////////////////////////////
// Linha shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('row', 'shortcode_row');
	function shortcode_row($atts, $content = null) {
	$out .= '<div class="row">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	
   return $out;

	}	

//////////////////////////////////////////////////////////////////
// Coluna Interna shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('colunasI', 'shortcode_colunasI');
	function shortcode_colunasI($atts, $content = null) {
		extract(shortcode_atts(array(
        'qtde'      => '#',
    ), $atts));
	$out .= '<div class="col-md-' . $qtde . '';
	$out .= '">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	
   return $out;

	}
	
//////////////////////////////////////////////////////////////////
// Linha Interna shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('rowI', 'shortcode_rowI');
	function shortcode_rowI($atts, $content = null) {
	$out .= '<div class="row">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	
   return $out;

	}	

//////////////////////////////////////////////////////////////////
// Container shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('container', 'shortcode_container');
	function shortcode_container($atts, $content = null) {
	$out .= '<section class="container">';
	$out .= do_shortcode($content);
	$out .= '</section>';
	
   return $out;

	}	


//////////////////////////////////////////////////////////////////
// Botão Download
//////////////////////////////////////////////////////////////////
add_shortcode('download', 'shortcode_loja');
	function shortcode_loja($atts) {
		$atts = shortcode_atts(
			array(
				'link' => '',
			), $atts);

           return '<p class="text-right"><a class="btn-download" href="'.$atts['link'].'" role="button">Download</a></p>';

	}

//////////////////////////////////////////////////////////////////
// Box faixa
//////////////////////////////////////////////////////////////////
add_shortcode('box', 'shortcode_box');
	function shortcode_box($atts, $content = null) {
	$out .= '<section class="orcamento">';
	$out .= '<div class="container">';
	$out .= '<div class="row">';
	$out .= '<div class="col-md-12">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	$out .= '</div>';
	$out .= '</div>';
	$out .= '</section>';
	
   return $out;

	}	


	
//////////////////////////////////////////////////////////////////
// Form Produtos
//////////////////////////////////////////////////////////////////
add_shortcode('formulario', 'shortcode_formulario');
	function shortcode_formulario($atts) {
		$atts = shortcode_atts(
			array(
				'Assunto' => ''
			), $atts);

           return '<section class="formulario-produtos">
		   			<h4 class="titulo-lateral text-center">Solicite um orçamento</h4>
		   <form>
  <div class="form-group">
    <label for="Nome">Nome</label>
    <input type="text" class="form-control" id="Nome" name="nome">
  </div>
  <div class="form-group">
    <label for="Email">Email</label>
    <input type="email" class="form-control" id="Email" name="email">
  </div>
  <div class="form-group">
    <label for="Telefone">Telefone</label>
    <input type="text" class="form-control" id="Telefone" name="telefone">
  </div>
  <div class="form-group">
    <label for="Mensagem">Mensagem</label>
	<textarea class="form-control" rows="4" id="mensagem" name="mensagem"></textarea>
  </div>
  <p class="text-right"><button type="submit" class="btn btn-default btn-laranja">Enviar</button></p>
  <input type="hidden" value="'.$atts['Assunto'].'" name="assunto">
</form>
</section>';

	}
	
	
//////////////////////////////////////////////////////////////////
// Slide Produtos
//////////////////////////////////////////////////////////////////
add_shortcode('slide', 'shortcode_slide');
	function shortcode_slide($atts) {
		$atts = shortcode_atts(
			array(
				'foto1' => '',
				'foto2' => '',
				'foto3' => '',
				'produto' => ''
			), $atts);

           return '	<div id="slide-interna" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#slide-interna" data-slide-to="0" class="active"></li>
    <li data-target="#slide-interna" data-slide-to="1"></li>
    <li data-target="#slide-interna" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="'.$atts['foto1'].'" alt="'.$atts['produnto'].'">
    </div>
    <div class="item">
      <img src="'.$atts['foto2'].'" alt="'.$atts['produnto'].'">
    </div>
    <div class="item">
      <img src="'.$atts['foto3'].'" alt="'.$atts['produnto'].'">
    </div>
  </div>
</div>
';
	}
	
//////////////////////////////////////////////////////////////////
// Table Responsive
//////////////////////////////////////////////////////////////////
add_shortcode('table', 'shortcode_table');
	function shortcode_table($atts, $content = null) {
	$out .= '<div class="table-responsive">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	
   return $out;

	}	
		
//////////////////////////////////////////////////////////////////
// Titulos e suas cores
//////////////////////////////////////////////////////////////////
add_shortcode('titulo', 'shortcode_titulo');
	function shortcode_titulo($atts, $content = null) {
		extract(shortcode_atts(array(
        'tamanho'      => '#',
        'class'      => '#'
    ), $atts));
	$out .= '<h'.$tamanho.' class="'.$class.'">';
	$out .= do_shortcode($content);
	$out .= '</h'.$tamanho.'>';
	
   return $out;

	}
		
//////////////////////////////////////////////////////////////////
// Modal
//////////////////////////////////////////////////////////////////

add_shortcode ('modal','shortcode_modal');
	function shortcode_modal($atts, $content = null){
		extract (shortcode_atts(array(
			'titulo'		=> '#',
			'id'		    => '#',
			'thumb'    		=> '#',
		), $atts));
		
		$out .='<a data-target="#'.$id.'" role="button" data-toggle="modal"><img src="'.$thumb.'" alt="'.$titulo.'"></a>';
		$out .='<div class="modal" id="'.$id.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none";>';
		$out .='<div class="modal-header">';
		$out .='<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>';
		$out .='<h3 id="myModalLabel">'.$titulo.'</h3>';
		$out .='</div>';
		$out .='<div class="modal-body">';
		$out .= do_shortcode($content);;
		$out .='</div>';
		$out .='<div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Fechar</button></div></div>';
	
   		return $out;

	}


//////////////////////////////////////////////////////////////////
// Toggle
//////////////////////////////////////////////////////////////////
add_shortcode ('toggle','shortcode_toggle');
	function shortcode_toggle($atts, $content = null){
	$out .='<div class="accordion" id="accordion2">';
	$out .= do_shortcode($content);
	$out .= '</div>';
	
   return $out;

	}	


//////////////////////////////////////////////////////////////////
// Itens Toggler
//////////////////////////////////////////////////////////////////

add_shortcode ('aba','shortcode_aba');
	function shortcode_aba($atts, $content = null){
		extract (shortcode_atts(array(
			'titulo'		=> '#',
			'slug'		=> '#',
		), $atts));
		
		$out .='<div class="accordion-group">';
		$out .='<div class="accordion-heading">';
		$out .='<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#'.$slug.'">';
		$out .= $titulo;
		$out .='</a>';
		$out .='</div>';
		$out .='<div id="'.$slug.'" class="accordion-body collapse">';
		$out .='<div class="accordion-inner">';
		$out .= do_shortcode($content);
		$out .='</div>';
		$out .='</div>';
		$out .='</div>';

   		return $out;

	}
	
//////////////////////////////////////////////////////////////////
// Pull Right
//////////////////////////////////////////////////////////////////

add_shortcode('pull-right', 'shortcode_pullr');
	function shortcode_pullr($atts, $content = null) {
	$out .= '<div class="pull-right">';
	$out .= do_shortcode($content);
	$out .= '</div><div class="clearfix"></div>';
	
   return $out;

	}	

//////////////////////////////////////////////////////////////////
// Pull Left
//////////////////////////////////////////////////////////////////

add_shortcode('pull-left', 'shortcode_pulll');
	function shortcode_pulll($atts, $content = null) {
	$out .= '<div class="pull-left">';
	$out .= do_shortcode($content);
	$out .= '</div><div class="clearfix"></div>';
	
   return $out;

	}	
	
//////////////////////////////////////////////////////////////////
// Botões
//////////////////////////////////////////////////////////////////
add_shortcode('btn', 'shortcode_btn');
	function shortcode_btn($atts, $content = null) {
		extract(shortcode_atts(array(
        'class'      => '#',
        'title'      => '#',
        'link'      => 'http://',
    ), $atts));
	$out .= '<a class="btn ' . $class . '';
	$out .= '" title="' .$title.'" href=" '.$link.'" role="button">';
	$out .= do_shortcode($content);
	$out .='<span class="glyphicon glyphicon-chevron-right"></span>';
	$out .= '</a>';
	
   return $out;

	}
	
//////////////////////////////////////////////////////////////////
// Botões Links Externos
//////////////////////////////////////////////////////////////////
add_shortcode('btn-e', 'shortcode_btn_e');
	function shortcode_btn_e($atts, $content = null) {
		extract(shortcode_atts(array(
        'class'      => '#',
        'title'      => '#',
        'link'      => 'http://',
    ), $atts));
	$out .= '<a class="btn ' . $class . '';
	$out .= '" title="' .$title.'" href=" '.$link.'" role="button" target="_blank">';
	$out .= do_shortcode($content);
	$out .='<span class="glyphicon glyphicon-chevron-right"></span>';
	$out .= '</a>';
	
   return $out;

	}


//////////////////////////////////////////////////////////////////
// Blockquote
//////////////////////////////////////////////////////////////////

add_shortcode('blockquote', 'shortcode_blockquote');
	function shortcode_blockquote($atts, $content = null) {
	$out .= '<blockquote>';
	$out .= do_shortcode($content);
	$out .= '</blockquote>';
	
   return $out;

	}	
//////////////////////////////////////////////////////////////////
// Tabs
//////////////////////////////////////////////////////////////////

add_shortcode ('tabs','shortcode_tabs');
	function shortcode_tabs($atts, $content = null){
	$out .='<ul class="nav nav-tabs">';
	$out .= do_shortcode($content);
	$out .= '</ul>';
	
   return $out;

	}	

//////////////////////////////////////////////////////////////////
// List Unstyled
//////////////////////////////////////////////////////////////////
add_shortcode('unstyled', 'shortcode_unstyled');
function shortcode_unstyled( $atts, $content = null ) {
	$content = str_replace('<ul>', '<ul class="list-unstyled">', do_shortcode($content));
	return $content;	
}



//////////////////////////////////////////////////////////////////
// Icon
//////////////////////////////////////////////////////////////////

add_shortcode('icon', 'shortcode_icon');
	function shortcode_icon($atts, $content = null) {
	$out .= '<i class="azul glyphicon glyphicon-';
	$out .= do_shortcode($content);
	$out .= '"></i>  ';
	
   return $out;

	}	

//////////////////////////////////////////////////////////////////
// Youtube shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('youtube', 'shortcode_youtube');
	function shortcode_youtube($atts) {
		$atts = shortcode_atts(
			array(
				'id' => '',
			), $atts);

           return '<div class="embed-responsive embed-responsive-16by9">
                            	<iframe class="embed-responsive-item" title="YouTube video player" src="http://www.youtube.com/embed/' . $atts['id'] . '" frameborder="0" allowfullscreen="" id="' . $atts['id'] . '"></iframe>
                    </div>';

	}

//////////////////////////////////////////////////////////////////
// Vimeo shortcode
//////////////////////////////////////////////////////////////////
add_shortcode('vimeo', 'shortcode_vimeo');
	function shortcode_vimeo($atts) {
		$atts = shortcode_atts(
			array(
				'id' => '',
			), $atts);

           return '<div class="embed-responsive embed-responsive-16by9">
                            	<iframe class="embed-responsive-item" title="Vimeo video player" src="https://player.vimeo.com/video/' . $atts['id'] . '" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen id="' . $atts['id'] . '"></iframe>
                    </div>';

	}

?>