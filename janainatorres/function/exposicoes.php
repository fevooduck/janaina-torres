<?php

add_action( 'init', 'exposicoes_type' );
function exposicoes_type() {
	register_post_type( 'exposicoes',
		array(
			'labels' => array(
				'name' 				=> 'Exposições',
				'singular_name' 	=> 'Exposição',
				'menu_name'         => 'Exposições',
				'all_items'         => 'Exposições',
				'view_item'         => 'Ver Exposição',
				'add_new_item'      => 'Adicionar nova Exposição',
				'add_new'           => 'Adicionar Exposição',
				'edit_item'         => 'Alterar Exposição',
				'update_item'       => 'Atualizar Exposição',
				'search_items'      => 'Pesquisar Exposição',
				'not_found'         => 'Nenhum Exposição Encontrado',
				'not_found_in_trash'=> 'Nenhum Exposição Encontrado na Lixeira',
			),
		'hierarchical' 		  => true,
		'has_archive' 		  => true,
		'public' 			  => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'menu_icon' 		  => 'dashicons-grid-view',
    	'menu_position' => 5,
		'supports'            => array( 'title', 'thumbnail', 'page-attributes'),
		'rewrite'             => array( 'slug' => 'exposicoes'),
    	)
	);
	
	flush_rewrite_rules();
}
 
function ep_exposicoesposts_metaboxes() {
	add_meta_box( 'titulo_exposicoes_add', 'Configuração Título', 'titulo_exposicoes_add', 'exposicoes', 'normal', 'default', array('id'=>'_add') );
	add_meta_box( 'subtitulo_exposicoes_add', 'Subtítulo', 'subtitulo_exposicoes_add', 'exposicoes', 'normal', 'default', array('id'=>'_add') );
	add_meta_box( 'data_exposicoes_add', 'Data', 'data_exposicoes_add', 'exposicoes', 'normal', 'default', array('id'=>'_add') );
}
add_action( 'admin_init', 'ep_exposicoesposts_metaboxes' );

function titulo_exposicoes_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_exposicoesposts_nonce' );

    // The metabox HTML
    $exposicoes_caixa = get_post_meta( $post->ID, '_exposicoes_caixa', true );
	$sim_checked = $exposicoes_caixa == "sim" ? ' selected="selected"' : '';
	$nao_checked = $exposicoes_caixa == "nao" ? ' selected="selected"' : '';
	
	echo '<p>Caixa Alta:</p>';
    echo '<select name="_exposicoes_caixa" id="meta_box_select">';
	echo '<option value="sim" '.$sim_checked.'>Sim</option>';
	echo '<option value="nao" '.$nao_checked.'>Não</option>';
	echo '</select>';

    $exposicoes_estiloT = get_post_meta( $post->ID, '_exposicoes_estiloT', true );
	$boldT_checked = $exposicoes_estiloT == "bold" ? ' selected="selected"' : '';
	$regularT_checked = $exposicoes_estiloT == "regular" ? ' selected="selected"' : '';
	$lightT_checked = $exposicoes_estiloT == "light" ? ' selected="selected"' : '';

	echo '<p>Estilo da Fonte:</p>';
	echo '<select name="_exposicoes_estiloT" id="meta_box_select">';
	echo '<option value="bold" '.$boldT_checked.'>Bold</option>';
	echo '<option value="regular" '.$regularT_checked.'>Regular</option>';
	echo '<option value="light" '.$lightT_checked.'>Light</option>';
	echo '</select>';
	
	echo '<p>Cor:</p>';
	$exposicoes_corT = get_post_meta( $post->ID, '_exposicoes_corT', true );
    echo ' <input class="color_field" type="text" name="_exposicoes_corT" value="' . $exposicoes_corT  . '" data-default-color="#555555"/>';
}

function subtitulo_exposicoes_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_exposicoesposts_nonce' );

	// The metabox HTML
	echo '<p>Subtítulo:</p>';
    $exposicoes_subtitulo = get_post_meta( $post->ID, '_exposicoes_subtitulo', true );
    echo '<input type="text" name="_exposicoes_subtitulo" value="' . $exposicoes_subtitulo  . '"  style="width:99%"/>';
    
	$exposicoes_estiloS = get_post_meta( $post->ID, '_exposicoes_estiloS', true );
	$boldS_checked = $exposicoes_estiloS == "bold" ? ' selected="selected"' : '';
	$regularS_checked = $exposicoes_estiloS == "regular" ? ' selected="selected"' : '';
	$lightS_checked = $exposicoes_estiloS == "light" ? ' selected="selected"' : '';

	echo '<p>Estilo da Fonte:</p>';
	echo '<select name="_exposicoes_estiloS" id="meta_box_select">';
	echo '<option value="regular" '.$regularS_checked.'>Regular</option>';
	echo '<option value="bold" '.$boldS_checked.'>Bold</option>';
	echo '<option value="light" '.$lightS_checked.'>Light</option>';
	echo '</select>';
	
	echo '<p>Cor:</p>';
	$exposicoes_corS = get_post_meta( $post->ID, '_exposicoes_corS', true );
    echo ' <input class="color_field" type="text" name="_exposicoes_corS" value="' . $exposicoes_corS  . '" data-default-color="#848383"/>';
}

function data_exposicoes_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_exposicoesposts_nonce' );
 	?>
   <script>
	   var $a = jQuery.noConflict();
		$a(document).ready(function($){
		    $('.color_field').each(function(){
        		$(this).wpColorPicker();
    		    });
		});
		</script>
   <?php 
    // The metabox HTML
	echo '<p>Data:</p>';
    $exposicoes_data = get_post_meta( $post->ID, '_exposicoes_data', true );
    echo '<input type="text" name="_exposicoes_data" value="' . $exposicoes_data  . '"  style="width:99%"/>';
    
	$exposicoes_estiloD = get_post_meta( $post->ID, '_exposicoes_estiloD', true );
	$boldD_checked = $exposicoes_estiloD == "bold" ? ' selected="selected"' : '';
	$regularD_checked = $exposicoes_estiloD == "regular" ? ' selected="selected"' : '';
	$lightD_checked = $exposicoes_estiloD == "light" ? ' selected="selected"' : '';

	echo '<p>Estilo da Fonte:</p>';
	echo '<select name="_exposicoes_estiloD" id="meta_box_select">';
	echo '<option value="bold" '.$boldD_checked.'>Bold</option>';
	echo '<option value="regular" '.$regularD_checked.'>Regular</option>';
	echo '<option value="light" '.$lightD_checked.'>Light</option>';
	echo '</select>';
	
	echo '<p>Cor:</p>';
	$exposicoes_corD = get_post_meta( $post->ID, '_exposicoes_corD', true );
    echo ' <input class="color_field" type="text" name="_exposicoes_corD" value="' . $exposicoes_corD  . '"  data-default-color="#555555"/>';
}

// Save the Metabox Data
function ep_exposicoesposts_save_meta( $post_id, $post ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['ep_exposicoesposts_nonce'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['ep_exposicoesposts_nonce'], plugin_basename( __FILE__ ) ) )
        return;
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ) )
        return;
 
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though
 	$exposicoes_meta_save['_exposicoes_caixa'] = $_POST['_exposicoes_caixa'];
 	$exposicoes_meta_save['_exposicoes_estiloT'] = $_POST['_exposicoes_estiloT'];
 	$exposicoes_meta_save['_exposicoes_corT'] = $_POST['_exposicoes_corT'];
 	$exposicoes_meta_save['_exposicoes_subtitulo'] = $_POST['_exposicoes_subtitulo'];
 	$exposicoes_meta_save['_exposicoes_estiloS'] = $_POST['_exposicoes_estiloS'];
 	$exposicoes_meta_save['_exposicoes_corS'] = $_POST['_exposicoes_corS'];
 	$exposicoes_meta_save['_exposicoes_data'] = $_POST['_exposicoes_data'];
 	$exposicoes_meta_save['_exposicoes_estiloD'] = $_POST['_exposicoes_estiloD'];
 	$exposicoes_meta_save['_exposicoes_corD'] = $_POST['_exposicoes_corD'];

    // Add values of $events_meta as custom fields
    foreach ( $exposicoes_meta_save as $key => $value ) { // Cycle through the $events_meta array!
        if ( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode( ',', (array)$value ); // If $value is an array, make it a CSV (unlikely)
        if ( get_post_meta( $post->ID, $key, false ) ) { // If the custom field already has a value
            update_post_meta( $post->ID, $key, $value );
        } else { // If the custom field doesn't have a value
            add_post_meta( $post->ID, $key, $value );
        }
        if ( !$value ) delete_post_meta( $post->ID, $key ); // Delete if blank
    }
}
add_action( 'save_post', 'ep_exposicoesposts_save_meta', 1, 2 );

add_action( 'init', 'vistas_type' );
function vistas_type() {
    register_post_type( 'vistas',
        array(
            'labels' => array(
                'name'              => 'Vistas',
                'singular_name'     => 'Vista',
                'menu_name'         => 'Vistas',
                'all_items'         => 'Vistas',
                'view_item'         => 'Ver Vista',
                'add_new_item'      => 'Adicionar nova Vista',
                'add_new'           => 'Adicionar Vista',
                'edit_item'         => 'Alterar Vista',
                'update_item'       => 'Atualizar Vista',
                'search_items'      => 'Pesquisar Vista',
                'not_found'         => 'Nenhuma Vista Encontrada',
                'not_found_in_trash'=> 'Nenhuma Vista Encontrada na Lixeira',
            ),
        'hierarchical'        => true,
        'has_archive'         => true,
        'public'              => true,
        'exclude_from_search' => true,
        'capability_type'     => 'post',
        'menu_icon'           => 'dashicons-visibility',
        'menu_position' => 5,
        'supports'            => array( 'title', 'thumbnail', 'page-attributes'),
        'rewrite'             => array( 'slug' => 'vistas'),
        )
    );
    
    flush_rewrite_rules();
}


?>