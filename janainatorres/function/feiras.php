<?php

add_action( 'init', 'feiras_type' );
function feiras_type() {
	register_post_type( 'feiras',
		array(
			'labels' => array(
				'name' 				=> 'Feiras',
				'singular_name' 	=> 'Feira',
				'menu_name'         => 'Feiras',
				'all_items'         => 'Feiras',
				'view_item'         => 'Ver Feira',
				'add_new_item'      => 'Adicionar nova Feira',
				'add_new'           => 'Adicionar Feira',
				'edit_item'         => 'Alterar Feira',
				'update_item'       => 'Atualizar Feira',
				'search_items'      => 'Pesquisar Feira',
				'not_found'         => 'Nenhum Feira Encontrado',
				'not_found_in_trash'=> 'Nenhum Feira Encontrado na Lixeira',
			),
		'hierarchical' 		  => true,
		'has_archive' 		  => true,
		'public' 			  => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'menu_icon' 		  => 'dashicons-grid-view',
    	'menu_position' => 5,
		'supports'            => array( 'title', 'thumbnail', 'page-attributes'),
		'rewrite'             => array( 'slug' => 'feiras'),
    	)
	);
	
	flush_rewrite_rules();
}
 
function ep_feirasposts_metaboxes() {
	add_meta_box( 'titulo_feiras_add', 'Configuração Título', 'titulo_feiras_add', 'feiras', 'normal', 'default', array('id'=>'_add') );
	add_meta_box( 'subtitulo_feiras_add', 'Subtítulo', 'subtitulo_feiras_add', 'feiras', 'normal', 'default', array('id'=>'_add') );
	add_meta_box( 'data_feiras_add', 'Data', 'data_feiras_add', 'feiras', 'normal', 'default', array('id'=>'_add') );
}
add_action( 'admin_init', 'ep_feirasposts_metaboxes' );

function titulo_feiras_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_feirasposts_nonce' );

    // The metabox HTML
    $feiras_caixa = get_post_meta( $post->ID, '_feiras_caixa', true );
	$sim_checked = $feiras_caixa == "sim" ? ' selected="selected"' : '';
	$nao_checked = $feiras_caixa == "nao" ? ' selected="selected"' : '';
	
	echo '<p>Caixa Alta:</p>';
    echo '<select name="_feiras_caixa" id="meta_box_select">';
	echo '<option value="sim" '.$sim_checked.'>Sim</option>';
	echo '<option value="nao" '.$nao_checked.'>Não</option>';
	echo '</select>';

    $feiras_estiloT = get_post_meta( $post->ID, '_feiras_estiloT', true );
	$boldT_checked = $feiras_estiloT == "bold" ? ' selected="selected"' : '';
	$regularT_checked = $feiras_estiloT == "regular" ? ' selected="selected"' : '';
	$lightT_checked = $feiras_estiloT == "light" ? ' selected="selected"' : '';

	echo '<p>Estilo da Fonte:</p>';
	echo '<select name="_feiras_estiloT" id="meta_box_select">';
	echo '<option value="bold" '.$boldT_checked.'>Bold</option>';
	echo '<option value="regular" '.$regularT_checked.'>Regular</option>';
	echo '<option value="light" '.$lightT_checked.'>Light</option>';
	echo '</select>';
	
	echo '<p>Cor:</p>';
	$feiras_corT = get_post_meta( $post->ID, '_feiras_corT', true );
    echo ' <input class="color_field" type="text" name="_feiras_corT" value="' . $feiras_corT  . '" data-default-color="#555555"/>';
}

function subtitulo_feiras_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_feirasposts_nonce' );

	// The metabox HTML
	echo '<p>Subtítulo:</p>';
    $feiras_subtitulo = get_post_meta( $post->ID, '_feiras_subtitulo', true );
    echo '<input type="text" name="_feiras_subtitulo" value="' . $feiras_subtitulo  . '"  style="width:99%"/>';
    
	$feiras_estiloS = get_post_meta( $post->ID, '_feiras_estiloS', true );
	$boldS_checked = $feiras_estiloS == "bold" ? ' selected="selected"' : '';
	$regularS_checked = $feiras_estiloS == "regular" ? ' selected="selected"' : '';
	$lightS_checked = $feiras_estiloS == "light" ? ' selected="selected"' : '';

	echo '<p>Estilo da Fonte:</p>';
	echo '<select name="_feiras_estiloS" id="meta_box_select">';
	echo '<option value="regular" '.$regularS_checked.'>Regular</option>';
	echo '<option value="bold" '.$boldS_checked.'>Bold</option>';
	echo '<option value="light" '.$lightS_checked.'>Light</option>';
	echo '</select>';
	
	echo '<p>Cor:</p>';
	$feiras_corS = get_post_meta( $post->ID, '_feiras_corS', true );
    echo ' <input class="color_field" type="text" name="_feiras_corS" value="' . $feiras_corS  . '" data-default-color="#848383"/>';
}

function data_feiras_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_feirasposts_nonce' );
 	?>
   <script>
	   var $a = jQuery.noConflict();
		$a(document).ready(function($){
		    $('.color_field').each(function(){
        		$(this).wpColorPicker();
    		    });
		});
		</script>
   <?php 
    // The metabox HTML
	echo '<p>Data:</p>';
    $feiras_data = get_post_meta( $post->ID, '_feiras_data', true );
    echo '<input type="text" name="_feiras_data" value="' . $feiras_data  . '"  style="width:99%"/>';
    
	$feiras_estiloD = get_post_meta( $post->ID, '_feiras_estiloD', true );
	$boldD_checked = $feiras_estiloD == "bold" ? ' selected="selected"' : '';
	$regularD_checked = $feiras_estiloD == "regular" ? ' selected="selected"' : '';
	$lightD_checked = $feiras_estiloD == "light" ? ' selected="selected"' : '';

	echo '<p>Estilo da Fonte:</p>';
	echo '<select name="_feiras_estiloD" id="meta_box_select">';
	echo '<option value="bold" '.$boldD_checked.'>Bold</option>';
	echo '<option value="regular" '.$regularD_checked.'>Regular</option>';
	echo '<option value="light" '.$lightD_checked.'>Light</option>';
	echo '</select>';
	
	echo '<p>Cor:</p>';
	$feiras_corD = get_post_meta( $post->ID, '_feiras_corD', true );
    echo ' <input class="color_field" type="text" name="_feiras_corD" value="' . $feiras_corD  . '"  data-default-color="#555555"/>';
}

// Save the Metabox Data
function ep_feirasposts_save_meta( $post_id, $post ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['ep_feirasposts_nonce'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['ep_feirasposts_nonce'], plugin_basename( __FILE__ ) ) )
        return;
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ) )
        return;
 
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though
 	$feiras_meta_save['_feiras_caixa'] = $_POST['_feiras_caixa'];
 	$feiras_meta_save['_feiras_estiloT'] = $_POST['_feiras_estiloT'];
 	$feiras_meta_save['_feiras_corT'] = $_POST['_feiras_corT'];
 	$feiras_meta_save['_feiras_subtitulo'] = $_POST['_feiras_subtitulo'];
 	$feiras_meta_save['_feiras_estiloS'] = $_POST['_feiras_estiloS'];
 	$feiras_meta_save['_feiras_corS'] = $_POST['_feiras_corS'];
 	$feiras_meta_save['_feiras_data'] = $_POST['_feiras_data'];
 	$feiras_meta_save['_feiras_estiloD'] = $_POST['_feiras_estiloD'];
 	$feiras_meta_save['_feiras_corD'] = $_POST['_feiras_corD'];

    // Add values of $events_meta as custom fields
    foreach ( $feiras_meta_save as $key => $value ) { // Cycle through the $events_meta array!
        if ( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode( ',', (array)$value ); // If $value is an array, make it a CSV (unlikely)
        if ( get_post_meta( $post->ID, $key, false ) ) { // If the custom field already has a value
            update_post_meta( $post->ID, $key, $value );
        } else { // If the custom field doesn't have a value
            add_post_meta( $post->ID, $key, $value );
        }
        if ( !$value ) delete_post_meta( $post->ID, $key ); // Delete if blank
    }
}
add_action( 'save_post', 'ep_feirasposts_save_meta', 1, 2 );


?>