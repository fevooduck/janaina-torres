<?php

add_action( 'init', 'obras_type' );
function obras_type() {
	register_post_type( 'obras',
		array(
			'labels' => array(
				'name' 				=> 'Obras',
				'singular_name' 	=> 'Obra',
				'menu_name'         => 'Obras',
				'all_items'         => 'Todos os Obras',
				'view_item'         => 'Ver Obra',
				'add_new_item'      => 'Adicionar novo Obra',
				'add_new'           => 'Adicionar Obra',
				'edit_item'         => 'Alterar Obra',
				'update_item'       => 'Atualizar Obra',
				'search_items'      => 'Pesquisar Obra',
				'not_found'         => 'Nenhum Obra Encontrado',
				'not_found_in_trash'=> 'Nenhum Obra Encontrado na Lixeira',
			),
		'hierarchical' 		  => true,
		'has_archive' 		  => true,
		'public' 			  => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'menu_icon' 		  => 'dashicons-format-image',
    	'menu_position' => 5,
		'supports'            => array( 'title', 'editor', 'thumbnail', 'page-attributes'),
		'rewrite'             => array( 'slug' => 'obras'),
    	)
	);
	
	flush_rewrite_rules();
}
 
function ep_obrasposts_metaboxes() {
	add_meta_box( 'ept_info_add', 'Informações da Obra', 'ept_info_add', 'obras', 'normal', 'default', array('id'=>'_add') );
	add_meta_box( 'ept_ano_add', 'Ano', 'ept_ano_add', 'obras', 'side', 'default', array('id'=>'_add') );
	add_meta_box( 'ept_artistas_add', 'Artistas', 'ept_artistas_add', 'obras', 'side', 'default', array('id'=>'_add') );
	add_meta_box( 'ept_serie_add', 'Séries', 'ept_serie_add', 'obras', 'side', 'default', array('id'=>'_add') );
	add_meta_box( 'ept_exposicao_add', 'Exposições', 'ept_exposicao_add', 'obras', 'side', 'default', array('id'=>'_add') );
	add_meta_box( 'ept_exclusivo_add', 'Exclusivo Exposições', 'ept_exclusivo_add', 'obras', 'side', 'default', array('id'=>'_add') );
	add_meta_box( 'ept_video_add', 'Video', 'ept_video_add', 'obras', 'side', 'default', array('id'=>'_add') );
}
add_action( 'admin_init', 'ep_obrasposts_metaboxes' );

function ept_info_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_obrasposts_nonce' );

	echo '<p>Técnica:</p>';
    $obras_tecnica = get_post_meta( $post->ID, '_obras_tecnica', true );
    echo '<input type="text" name="_obras_tecnica" value="' . $obras_tecnica  . '"  style="width:99%"/>';

	echo '<p>Dimensão 1:</p>';
    $obras_dimensao1 = get_post_meta( $post->ID, '_obras_dimensao1', true );
    echo '<input type="text" name="_obras_dimensao1" value="' . $obras_dimensao1  . '"  style="width:99%"/>';

	echo '<p>Dimensão 2:</p>';
    $obras_dimensao2 = get_post_meta( $post->ID, '_obras_dimensao2', true );
    echo '<input type="text" name="_obras_dimensao2" value="' . $obras_dimensao2  . '"  style="width:99%"/>';

	echo '<p>Dimensão 3:</p>';
    $obras_dimensao3 = get_post_meta( $post->ID, '_obras_dimensao3', true );
    echo '<input type="text" name="_obras_dimensao3" value="' . $obras_dimensao3  . '"  style="width:99%"/>';

}

function ept_artistas_add() {
     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_obrasposts_nonce' );
	
	$meta = get_post_meta( $post->ID );

	$obras_artista = ( isset( $meta['obras_artista'][0] ) && '' !== $meta['obras_artista'][0] ) ? $meta['obras_artista'][0] : '';
	
	$args_artistas = array(
			'post_type' 			=> 'artistas', 
			'selected'              => $obras_artista,
			'echo'                  => 1,
			'name'                  => 'obras_artista',
			'id'                    => 'obras_artista',
			'class'                 => null,
			'show_option_none'      => null,
			'show_option_no_change' => 'Escolha um artista',
			'option_none_value'     => 'Escolha um artista',

		);

		wp_dropdown_pages( $args_artistas );

} 

function ept_serie_add() {
     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_obrasposts_nonce' );
	
	$meta = get_post_meta( $post->ID );

	$obras_series = ( isset( $meta['obras_series'][0] ) && '' !== $meta['obras_series'][0] ) ? $meta['obras_series'][0] : '';
	
	$args_series = array(
			'post_type' 			=> 'series', 
			'selected'              => $obras_series,
			'echo'                  => 1,
			'name'                  => 'obras_series',
			'id'                    => 'obras_series',
			'class'                 => null,
			'show_option_none'      => null,
			'show_option_no_change' => 'Escolha um série',
			'option_none_value'     => 'Escolha um série',

		);

		wp_dropdown_pages( $args_series );

} 

function ept_exposicao_add() {
     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_obrasposts_nonce' );
	
	$meta = get_post_meta( $post->ID );

	$obras_exposicao = ( isset( $meta['obras_exposicao'][0] ) && '' !== $meta['obras_exposicao'][0] ) ? $meta['obras_exposicao'][0] : '';
	
	$args_exposicao = array(
			'post_type' 			=> 'exposicoes', 
			'selected'              => $obras_exposicao,
			'echo'                  => 1,
			'name'                  => 'obras_exposicao',
			'id'                    => 'obras_exposicao',
			'class'                 => null,
			'show_option_none'      => null,
			'show_option_no_change' => 'Escolha uma exposição',
			'option_none_value'     => 'Escolha uma exposição',

		);

		wp_dropdown_pages( $args_exposicao );

} 

function ept_exclusivo_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_obrasposts_nonce' );

    // The metabox HTML
    $obras_exclusivo = get_post_meta( $post->ID, '_obras_exclusivo', true );
	$sim_checked = $obras_exclusivo == "sim" ? ' selected="selected"' : '';
	$nao_checked = $obras_exclusivo == "nao" ? ' selected="selected"' : '';
	
	echo '<p>Exclusivo:</p>';
    echo '<select name="_obras_exclusivo" id="meta_box_select">';
	echo '<option value="nao" '.$nao_checked.'>Não</option>';
	echo '<option value="sim" '.$sim_checked.'>Sim</option>';
	echo '</select>';
}

function ept_video_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_obrasposts_nonce' );

    // The metabox HTML
    $obras_video = get_post_meta( $post->ID, '_obras_video', true );
	$sim_checked = $obras_video == "sim" ? ' selected="selected"' : '';
	$nao_checked = $obras_video == "nao" ? ' selected="selected"' : '';
	
	echo '<p>Somente vídeo:</p>';
    echo '<select name="_obras_video" id="meta_box_select">';
	echo '<option value="nao" '.$nao_checked.'>Não</option>';
	echo '<option value="sim" '.$sim_checked.'>Sim</option>';
	echo '</select>';
}

function ept_ano_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_obrasposts_nonce' );

    // The metabox HTML
	echo '<p>Ano:</p>';
    $obras_ano = get_post_meta( $post->ID, '_obras_ano', true );
    echo '<input type="text" name="_obras_ano" value="' . $obras_ano  . '"  style="width:99%"/>';
}

// Save the Metabox Data
function ep_obrasposts_save_meta( $post_id, $post ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['ep_obrasposts_nonce'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['ep_obrasposts_nonce'], plugin_basename( __FILE__ ) ) )
        return;
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ) )
        return;
 
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though
 	$obras_meta_save['_obras_tecnica'] = $_POST['_obras_tecnica'];
 	$obras_meta_save['_obras_dimensao1'] = $_POST['_obras_dimensao1'];
 	$obras_meta_save['_obras_dimensao2'] = $_POST['_obras_dimensao2'];
 	$obras_meta_save['_obras_dimensao3'] = $_POST['_obras_dimensao3'];
 	$obras_meta_save['_obras_dimensao3'] = $_POST['_obras_dimensao3'];
 	$obras_meta_save['_obras_exclusivo'] = $_POST['_obras_exclusivo'];
 	$obras_meta_save['_obras_video'] = $_POST['_obras_video'];
 	$obras_meta_save['_obras_ano'] = $_POST['_obras_ano'];

    // Add values of $obras_meta as custom fields
    foreach ( $obras_meta_save as $key => $value ) { // Cycle through the $obras_meta array!
        if ( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode( ',', (array)$value ); // If $value is an array, make it a CSV (unlikely)
        if ( get_post_meta( $post->ID, $key, false ) ) { // If the custom field already has a value
            update_post_meta( $post->ID, $key, $value );
        } else { // If the custom field doesn't have a value
            add_post_meta( $post->ID, $key, $value );
        }
		if ( isset( $_POST['obras_artista'] ) ) { // Input var okay.
			update_post_meta( $post->ID, 'obras_artista', sanitize_text_field( wp_unslash( $_POST['obras_artista'] ) ) ); // Input var okay.
		}
		if ( isset( $_POST['obras_series'] ) ) { // Input var okay.
			update_post_meta( $post->ID, 'obras_series', sanitize_text_field( wp_unslash( $_POST['obras_series'] ) ) ); // Input var okay.
		}
		if ( isset( $_POST['obras_exposicao'] ) ) { // Input var okay.
			update_post_meta( $post->ID, 'obras_exposicao', sanitize_text_field( wp_unslash( $_POST['obras_exposicao'] ) ) ); // Input var okay.
		}
        if ( !$value ) delete_post_meta( $post->ID, $key ); // Delete if blank
    }
}
add_action( 'save_post', 'ep_obrasposts_save_meta', 1, 2 );

// ADD COLUMNS POST TYPE ADMIN AREA
/* Création de la colonne 
add_filter('manage_obras_posts_columns', 'posts_columns_obras', 10, 1);
function posts_columns_obras($columns){
	$columns = array(
		'cb' => '<input type="checkbox" />',
		'title' => __('Obra'),
    	'obra_artista' => 'Artista',
    	'obra_serie' => 'Série',
    );
    return $columns;
}
 
add_action('manage_obras_posts_custom_column', 'posts_custom_columns_obras', 5, 2);
function posts_custom_columns_obras($column_name, $post_id){
	$obra_artista = get_post_meta($post_id,'obras_artista', true);
	$obra_serie = get_post_meta($post_id,'obras_series', true);
	$nome_artista = get_post( $obra_artista ); 
	$titulo_artista = $nome_artista->post_title;
	
	if($column_name === 'obra_artista'){
        echo $titulo_artista;  
    }
	
	if($column_name === 'obra_serie'){
        if($obra_serie == -1){ 
			echo "-";	
		}else{
			$nome_serie = get_post( $obra_serie ); 
			$titulo_serie = $nome_serie->post_title;
			echo $titulo_serie;
		}
    }
}
*/
add_filter('manage_obras_posts_columns', 'obras_admin_column_list');

function obras_admin_column_list($columns) { 
  unset($columns['date']); 
  // Delete the element from the array and the column disappears.
  unset($columns['title']);  
  // Delete title so I can add it back in my preferred order.
  $columns['title'] = 'Nome da Obra'; 
  // Adding back the built-in 'title' column, but giving it a new label.
  $columns['artista'] = 'Artista';
  $columns['serie'] = 'Série';
  $columns['modified'] = 'Última Modificação';
  return $columns;
}

add_action( 'manage_obras_posts_custom_column' , 'obras_custom_columns', 10, 2 );

function obras_custom_columns( $column, $post_id ) {
  	$obra_artista = get_post_meta($post_id,'obras_artista', true);
	$obra_serie = get_post_meta($post_id,'obras_series', true);
	$nome_artista = get_post( $obra_artista ); 
	$titulo_artista = $nome_artista->post_title;

  if ($column == 'artista') {
        echo $titulo_artista;  
  }
  if ($column == 'serie') {
        if($obra_serie == -1){ 
			echo "-";	
		}else{
			$nome_serie = get_post( $obra_serie ); 
			$titulo_serie = $nome_serie->post_title;
			echo $titulo_serie;
		}
  }

  // For 'modified', I'm adapting some formatting logic from Andrew Norcross
  // http://andrewnorcross.com/tutorials/modified-date-display/
  // to provide pretty formatting and include the name of the last editor.
  if ($column == 'modified') {    
    $m_orig = get_post_field( 'post_modified', $post_id, 'raw' );
    $m_stamp = strtotime( $m_orig );
    $modified	= date('j/n/y @ g:i a', $m_stamp );
    $modr_id	= get_post_meta( $post_id, '_edit_last', true );
    $auth_id	= get_post_field( 'post_author', $post_id, 'raw' );
    $user_id	= !empty( $modr_id ) ? $modr_id : $auth_id;
    $user_info	= get_userdata( $user_id );
    echo '<p class="mod-date">' . $modified. '<br />';
    echo 'por <strong>' . $user_info->display_name . '</strong>';
    echo '</p>';
  }
}

add_filter( 'manage_edit-obras_sortable_columns' , 'obras_admin_sortable_columns' );

function obras_admin_sortable_columns($columns) {
  $columns['modified'] = 'modified';
  $columns['artista'] = 'artista';
  $columns['serie'] = 'serie';
  return $columns;
}

add_filter( 'pre_get_posts', 'obras_admin_sort_columns_by');
function obras_admin_sort_columns_by( $query ) {  
  if( ! is_admin() ) { 
    // we don't want to affect public-facing pages
    return;  
  }
  $orderby = $query->get( 'orderby');  
  if( 'artista' == $orderby ) {  
    $query->set('meta_key', 'obras_artista');  
    $query->set('orderby','meta_value');  
  }
  if( 'serie' == $orderby ) {  
    $query->set('meta_key', 'obras_series');  
    $query->set('orderby','meta_value');  
  }
}



?>