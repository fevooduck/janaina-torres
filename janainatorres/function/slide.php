<?php

add_action( 'init', 'slide_post_type' );
function slide_post_type() {
	register_post_type( 'slide',
		array(
			'labels' => array(
				'name' 				=> 'Slides',
				'singular_name' 	=> 'Slides',
				'menu_name'         => 'Slides',
				'all_items'         => 'Todos os Slides',
				'view_item'         => 'Ver Slide',
				'add_new_item'      => 'Adicionar novo Slide',
				'add_new'           => 'Adicionar Slide',
				'edit_item'         => 'Alterar Slide',
				'update_item'       => 'Atualizar Slide',
				'search_items'      => 'Pesquisar Slide',
				'not_found'         => 'Nenhum Slide Encontrado',
				'not_found_in_trash'=> 'Nenhum Slide Encontrado na Lixeira',
			),
		'hierarchical' 		  => true,
		'has_archive' 		  => true,
		'public' 			  => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'menu_icon' 		  => 'dashicons-images-alt',
    	'menu_position' => 5,
		'supports'            => array( 'title', 'thumbnail', 'page-attributes'),
		'rewrite'             => array( 'slug' => 'slides'),
    	)
	);
	
	flush_rewrite_rules();
}
 
function ep_slideposts_metaboxes() {
	add_meta_box( 'ept_titulo_add', 'Configuração Título', 'ept_titulo_add', 'slide', 'normal', 'default', array('id'=>'_add') );
	add_meta_box( 'ept_subtitulo_add', 'Subtítulo', 'ept_subtitulo_add', 'slide', 'normal', 'default', array('id'=>'_add') );
	add_meta_box( 'ept_data_add', 'Data', 'ept_data_add', 'slide', 'normal', 'default', array('id'=>'_add') );
	add_meta_box( 'ept_link_add', 'Link', 'ept_link_add', 'slide', 'normal', 'default', array('id'=>'_add') );
}
add_action( 'admin_init', 'ep_slideposts_metaboxes' );

function ept_titulo_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_slideposts_nonce' );

    // The metabox HTML
    $slide_caixa = get_post_meta( $post->ID, '_slide_caixa', true );
	$sim_checked = $slide_caixa == "sim" ? ' selected="selected"' : '';
	$nao_checked = $slide_caixa == "nao" ? ' selected="selected"' : '';
	
	echo '<p>Caixa Alta:</p>';
    echo '<select name="_slide_caixa" id="meta_box_select">';
	echo '<option value="sim" '.$sim_checked.'>Sim</option>';
	echo '<option value="nao" '.$nao_checked.'>Não</option>';
	echo '</select>';

    $slide_estiloT = get_post_meta( $post->ID, '_slide_estiloT', true );
	$boldT_checked = $slide_estiloT == "bold" ? ' selected="selected"' : '';
	$regularT_checked = $slide_estiloT == "regular" ? ' selected="selected"' : '';
	$lightT_checked = $slide_estiloT == "light" ? ' selected="selected"' : '';

	echo '<p>Estilo da Fonte:</p>';
	echo '<select name="_slide_estiloT" id="meta_box_select">';
	echo '<option value="bold" '.$boldT_checked.'>Bold</option>';
	echo '<option value="regular" '.$regularT_checked.'>Regular</option>';
	echo '<option value="light" '.$lightT_checked.'>Light</option>';
	echo '</select>';
	
	echo '<p>Cor:</p>';
	$slide_corT = get_post_meta( $post->ID, '_slide_corT', true );
    echo ' <input class="color_field" type="text" name="_slide_corT" value="' . $slide_corT  . '" data-default-color="#555555"/>';
}

function ept_subtitulo_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_slideposts_nonce' );

	// The metabox HTML
	echo '<p>Subtítulo:</p>';
    $slide_subtitulo = get_post_meta( $post->ID, '_slide_subtitulo', true );
    echo '<input type="text" name="_slide_subtitulo" value="' . $slide_subtitulo  . '"  style="width:99%"/>';
    
	$slide_estiloS = get_post_meta( $post->ID, '_slide_estiloS', true );
	$boldS_checked = $slide_estiloS == "bold" ? ' selected="selected"' : '';
	$regularS_checked = $slide_estiloS == "regular" ? ' selected="selected"' : '';
	$lightS_checked = $slide_estiloS == "light" ? ' selected="selected"' : '';

	echo '<p>Estilo da Fonte:</p>';
	echo '<select name="_slide_estiloS" id="meta_box_select">';
	echo '<option value="regular" '.$regularS_checked.'>Regular</option>';
	echo '<option value="bold" '.$boldS_checked.'>Bold</option>';
	echo '<option value="light" '.$lightS_checked.'>Light</option>';
	echo '</select>';
	
	echo '<p>Cor:</p>';
	$slide_corS = get_post_meta( $post->ID, '_slide_corS', true );
    echo ' <input class="color_field" type="text" name="_slide_corS" value="' . $slide_corS  . '" data-default-color="#848383"/>';
}

function ept_data_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_slideposts_nonce' );
 	?>
   <script>
	   var $a = jQuery.noConflict();
		$a(document).ready(function($){
		    $('.color_field').each(function(){
        		$(this).wpColorPicker();
    		    });
		});
		</script>
   <?php 
    // The metabox HTML
	echo '<p>Data:</p>';
    $slide_data = get_post_meta( $post->ID, '_slide_data', true );
    echo '<input type="text" name="_slide_data" value="' . $slide_data  . '"  style="width:99%"/>';
    
	$slide_estiloD = get_post_meta( $post->ID, '_slide_estiloD', true );
	$boldD_checked = $slide_estiloD == "bold" ? ' selected="selected"' : '';
	$regularD_checked = $slide_estiloD == "regular" ? ' selected="selected"' : '';
	$lightD_checked = $slide_estiloD == "light" ? ' selected="selected"' : '';

	echo '<p>Estilo da Fonte:</p>';
	echo '<select name="_slide_estiloD" id="meta_box_select">';
	echo '<option value="bold" '.$boldD_checked.'>Bold</option>';
	echo '<option value="regular" '.$regularD_checked.'>Regular</option>';
	echo '<option value="light" '.$lightD_checked.'>Light</option>';
	echo '</select>';
	
	echo '<p>Cor:</p>';
	$slide_corD = get_post_meta( $post->ID, '_slide_corD', true );
    echo ' <input class="color_field" type="text" name="_slide_corD" value="' . $slide_corD  . '"  data-default-color="#555555"/>';
}

function ept_link_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_slideposts_nonce' );

	echo '<p>Link:</p>';
	$slide_link = get_post_meta( $post->ID, '_slide_link', true );
    echo '<input type="text" name="_slide_link" value="' . $slide_link  . '"  style="width:99%"/>';
}


// Save the Metabox Data
function ep_slideposts_save_meta( $post_id, $post ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['ep_slideposts_nonce'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['ep_slideposts_nonce'], plugin_basename( __FILE__ ) ) )
        return;
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ) )
        return;
 
    // OK, we're authenticated: we need to find and save the data
    // We'll put it into an array to make it easier to loop though
 	$slide_meta_save['_slide_caixa'] = $_POST['_slide_caixa'];
 	$slide_meta_save['_slide_estiloT'] = $_POST['_slide_estiloT'];
 	$slide_meta_save['_slide_corT'] = $_POST['_slide_corT'];
 	$slide_meta_save['_slide_subtitulo'] = $_POST['_slide_subtitulo'];
 	$slide_meta_save['_slide_estiloS'] = $_POST['_slide_estiloS'];
 	$slide_meta_save['_slide_corS'] = $_POST['_slide_corS'];
 	$slide_meta_save['_slide_data'] = $_POST['_slide_data'];
 	$slide_meta_save['_slide_estiloD'] = $_POST['_slide_estiloD'];
 	$slide_meta_save['_slide_corD'] = $_POST['_slide_corD'];
 	$slide_meta_save['_slide_link'] = $_POST['_slide_link'];
 	
    // Add values of $events_meta as custom fields
    foreach ( $slide_meta_save as $key => $value ) { // Cycle through the $events_meta array!
        if ( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode( ',', (array)$value ); // If $value is an array, make it a CSV (unlikely)
        if ( get_post_meta( $post->ID, $key, false ) ) { // If the custom field already has a value
            update_post_meta( $post->ID, $key, $value );
        } else { // If the custom field doesn't have a value
            add_post_meta( $post->ID, $key, $value );
        }
        if ( !$value ) delete_post_meta( $post->ID, $key ); // Delete if blank
    }
}
add_action( 'save_post', 'ep_slideposts_save_meta', 1, 2 );
?>