<?php
/* Séries */

add_action( 'init', 'series_post_type' );
function series_post_type() {
	register_post_type( 'series',
		array(
			'labels' => array(
				'name' 				=> 'Séries',
				'singular_name' 	=> 'Série',
				'menu_name'         => 'Séries',
				'all_items'         => 'Séries',
				'view_item'         => 'Ver Séries',
				'add_new_item'      => 'Adicionar novo Série',
				'add_new'           => 'Adicionar Série',
				'edit_item'         => 'Alterar Série',
				'update_item'       => 'Atualizar Série',
				'search_items'      => 'Pesquisar Série',
				'not_found'         => 'Nenhum Série Encontrado',
				'not_found_in_trash'=> 'Nenhum Série Encontrado na Lixeira',
			),
		'hierarchical' 		  => true,
		'has_archive' 		  => true,
		'public' 			  => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'menu_icon' 		  => 'dashicons-images-alt2',
    	'menu_position' => 5,
		'supports'            => array( 'title','thumbnail', 'editor', 'excerpt', 'page-attributes'),
		'rewrite'             => array( 'slug' => 'series'),
    	)
	);
	
	flush_rewrite_rules();
}

function ep_seriesposts_metaboxes() {
	add_meta_box( 'ept_artistas_serie_add', 'Artistas', 'ept_artistas_serie_add', 'series', 'side', 'default', array('id'=>'_add') );
	add_meta_box( 'ept_ano_serie_add', 'Ano', 'ept_ano_serie_add', 'series', 'side', 'default', array('id'=>'_add') );
}
add_action( 'admin_init', 'ep_seriesposts_metaboxes' );

function ept_artistas_serie_add() {
     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_seriesposts_nonce' );
	
	$meta = get_post_meta( $post->ID );

	$series_artista = ( isset( $meta['series_artista'][0] ) && '' !== $meta['series_artista'][0] ) ? $meta['series_artista'][0] : '';
	
	$args_artistas = array(
			'post_type' 			=> 'artistas', 
			'selected'              => $series_artista,
			'echo'                  => 1,
			'name'                  => 'series_artista',
			'id'                    => 'series_artista',
			'class'                 => null,
			'show_option_none'      => null,
			'show_option_no_change' => 'Escolha um artista',
			'option_none_value'     => 'Escolha um artista',

		);

		wp_dropdown_pages( $args_artistas );

} 

function ept_ano_serie_add() {

     global $post;
    // Use nonce for verification
    wp_nonce_field( plugin_basename( __FILE__ ), 'ep_seriesposts_nonce' );

    // The metabox HTML
	echo '<p>Ano:</p>';
    $series_ano = get_post_meta( $post->ID, '_series_ano', true );
    echo '<input type="text" name="_series_ano" value="' . $series_ano  . '"  style="width:99%"/>';
}


// Save the Metabox Data
function ep_seriesposts_save_meta( $post_id, $post ) {
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( !isset( $_POST['ep_seriesposts_nonce'] ) )
        return;
    if ( !wp_verify_nonce( $_POST['ep_seriesposts_nonce'], plugin_basename( __FILE__ ) ) )
        return;
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ) )
        return;
 
 	$series_meta_save['_series_ano'] = $_POST['_series_ano'];

    // Add values of $events_meta as custom fields
    foreach ( $series_meta_save as $key => $value ) { // Cycle through the $events_meta array!
        if ( $post->post_type == 'revision' ) return; // Don't store custom data twice
        $value = implode( ',', (array)$value ); // If $value is an array, make it a CSV (unlikely)
        if ( get_post_meta( $post->ID, $key, false ) ) { // If the custom field already has a value
            update_post_meta( $post->ID, $key, $value );
        } else { // If the custom field doesn't have a value
            add_post_meta( $post->ID, $key, $value );
        }
		if ( isset( $_POST['series_artista'] ) ) { // Input var okay.
			update_post_meta( $post->ID, 'series_artista', sanitize_text_field( wp_unslash( $_POST['series_artista'] ) ) ); // Input var okay.
		}
        if ( !$value ) delete_post_meta( $post->ID, $key ); // Delete if blank
    }
}
add_action( 'save_post', 'ep_seriesposts_save_meta', 1, 2 );


?>