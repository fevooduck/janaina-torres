<?php get_header(); 

/* Template Name: Newsletter */

?>
<?php if (have_posts()) : ?>
<?php while (have_posts()) : the_post(); ?>
<section class="internas page">
	<div class="container">
        <div class="row">
        	<div class="col-md-8 col-md-offset-2">
        	<?php the_content('Read the rest of this entry &raquo;'); ?>
<form action="//janainatorres.us14.list-manage.com/subscribe/post?u=2ba70db8f18925241295e0a53&amp;id=c8d0359a3a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
<div class="form-group">
	<label for="mce-FNAME">Nome </label>
	<input type="text" value="" name="FNAME" class="form-control " id="mce-FNAME">
</div>
<div class="form-group">
	<label for="mce-LNAME">Sobrenome </label>
	<input type="text" value="" name="LNAME" class="form-control " id="mce-LNAME">
</div>

<div class="form-group">
	<label for="mce-EMAIL">Email<span class="asterisk">*</span>
</label>
	<input type="email" value="" name="EMAIL" class="form-control required email" id="mce-EMAIL">
</div>
<p><span class="asterisk">*</span> campos obrigatórios</p>
	<div id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_45e7d30d0ae2e106769a72afc_06fd374f42" tabindex="-1" value=""></div>
    <div class="clearfix"><input type="submit" value="Inscreva-se" name="subscribe" id="mc-embedded-subscribe" class="btn-jtorres"></div>
    </div>
</form>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';fnames[0]='EMAIL';ftypes[0]='email'; /**
 * Translated default messages for the $ validation plugin.
 * Locale: PT_PT
 */
$.extend($.validator.messages, {
	required: "Campo de preenchimento obrigat&oacute;rio.",
	remote: "Por favor, corrija este campo.",
	email: "Por favor, introduza um endere&ccedil;o eletr&oacute;nico v&aacute;lido.",
	url: "Por favor, introduza um URL v&aacute;lido.",
	date: "Por favor, introduza uma data v&aacute;lida.",
	dateISO: "Por favor, introduza uma data v&aacute;lida (ISO).",
	number: "Por favor, introduza um n&uacute;mero v&aacute;lido.",
	digits: "Por favor, introduza apenas d&iacute;gitos.",
	creditcard: "Por favor, introduza um n&uacute;mero de cart&atilde;o de cr&eacute;dito v&aacute;lido.",
	equalTo: "Por favor, introduza de novo o mesmo valor.",
	accept: "Por favor, introduza um ficheiro com uma extens&atilde;o v&aacute;lida.",
	maxlength: $.validator.format("Por favor, n&atilde;o introduza mais do que {0} caracteres."),
	minlength: $.validator.format("Por favor, introduza pelo menos {0} caracteres."),
	rangelength: $.validator.format("Por favor, introduza entre {0} e {1} caracteres."),
	range: $.validator.format("Por favor, introduza um valor entre {0} e {1}."),
	max: $.validator.format("Por favor, introduza um valor menor ou igual a {0}."),
	min: $.validator.format("Por favor, introduza um valor maior ou igual a {0}.")
});}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
</div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</section>
  <?php get_footer(); ?>