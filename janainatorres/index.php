<?php get_header();?>
<section class="slider">
  <div class="container">
  	<div class="row">
  		<div class="col-md-10 col-md-offset-1">

  <div id="carousel-slide-home" class="carousel slide carousel-fade" data-ride="carousel">
  
    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
    <?php $cont=0;?>
    <?php $wp_query = new WP_Query(array( 
          'post_type' 		=> 'slide',
          'orderby'           => 'menu_order',
          'order' 		    => 'ASC',
          'posts_per_page'    => 5
  ));
  while ( have_posts() ) : the_post();
  $cont++ ?>
  <?php
      $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
      $titulo = get_the_title();
      $caixa = get_post_meta( $post->ID, '_slide_caixa', true );
      $estiloT = get_post_meta( $post->ID, '_slide_estiloT', true );
	  $corT = get_post_meta( $post->ID, '_slide_corT', true );
      $subtitulo = get_post_meta( $post->ID, '_slide_subtitulo', true );
      $estiloS = get_post_meta( $post->ID, '_slide_estiloS', true );
	  $corS = get_post_meta( $post->ID, '_slide_corS', true );
      $data = get_post_meta( $post->ID, '_slide_data', true );
	  $estiloD = get_post_meta( $post->ID, '_slide_estiloD', true );  
  	  $corD = get_post_meta( $post->ID, '_slide_corD', true );
  	  $link = get_post_meta( $post->ID, '_slide_link', true );
		
	if($estiloT == "bold"){
		$estiloTitle = "slide-bold";
	}elseif($estiloT == "regular"){
		$estiloTitle = "slide-regular";
	}elseif($estiloT == "light"){
		$estiloTitle = "slide-light";
	};
	
	if($estiloS == "bold"){
		$estiloSub = "slide-bold";
	}elseif($estiloS == "regular"){
		$estiloSub = "slide-regular";
	}elseif($estiloS == "light"){
		$estiloSub = "slide-light";
	};
	
	if($estiloD == "bold"){
		$estiloDate = "slide-bold";
	}elseif($estiloD == "regular"){
		$estiloDate = "slide-regular";
	}elseif($estiloD == "light"){
		$estiloDate = "slide-light";
	};
   ?>
      <div class="item <?php if($cont==1){?>active<?php } ?>">
          <a href="<?php echo $link; ?>"><img src="<?php echo $html[0]; ?>" alt="<?php the_title(); ?>" class="img-responsive"></a>
          <div class="text-right">
          	<h2 class="titulo-slide <?php echo $estiloTitle; ?>"><a href="<?php echo $link; ?>"  style="color:<?php echo $corT; ?>" ><span <?php if($caixa == 'sim'){?>class="text-uppercase"<?php } ?>><?php echo $titulo; ?></span> <span style="color:<?php echo $corS; ?>" class="subtitulo-slide <?php echo $estiloSub; ?>"><?php echo $subtitulo; ?></span></a></h2>
          	<p class="data-slide <?php echo $estiloDate; ?>"><a href="<?php echo $link; ?>"  style="color:<?php echo $corD; ?>"><?php echo $data; ?></a></p>
          </div>
      </div>
  <?php
  
  
  wp_reset_postdata();
  
  endwhile; 
  
  $totalcont = $cont;
  
  $numCont = $totalcont - 1;
  ?>
  </div>  
      <?php if($totalcont >= 2){ ?>
   <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#carousel-slide-home" data-slide-to="0" class="active"></li>
      <?php for ($valor = 1; $valor <= $numCont; $valor++){
      ?>
      <li data-target="#carousel-slide-home" data-slide-to="<?php echo $valor;?>"></li>
      <?php } ?>
    </ol>
      <?php } ?>
    
  </div>
    			
  		</div>
  	</div>
  </div>

</section>
<?php get_footer(); ?>