<?php get_header(); ?>
<section class="internas">
	<div class="container">
        <div class="row">
        	<div class="col-md-10 col-md-offset-1">
		<?php if (have_posts()) :
			while (have_posts()) : the_post();
			  $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
			  $titulo = get_the_title();
			  $permalink = get_the_permalink();
			  $caixa = get_post_meta( $post->ID, '_exposicoes_caixa', true );
			  $estiloT = get_post_meta( $post->ID, '_exposicoes_estiloT', true );
			  $corT = get_post_meta( $post->ID, '_exposicoes_corT', true );
			  $subtitulo = get_post_meta( $post->ID, '_exposicoes_subtitulo', true );
			  $estiloS = get_post_meta( $post->ID, '_exposicoes_estiloS', true );
			  $corS = get_post_meta( $post->ID, '_exposicoes_corS', true );

			if($estiloT == "bold"){
				$estiloTitle = "exposicoes-bold";
			}elseif($estiloT == "regular"){
				$estiloTitle = "exposicoes-regular";
			}elseif($estiloT == "light"){
				$estiloTitle = "exposicoes-light";
			};

			if($estiloS == "bold"){
				$estiloSub = "exposicoes-bold";
			}elseif($estiloS == "regular"){
				$estiloSub = "exposicoes-regular";
			}elseif($estiloS == "light"){
				$estiloSub = "exposicoes-light";
			};
		?>
 				<div class="row margin-exposicao">
				  <div class="col-md-12">
				  		<h2 class="text-right data-archive-exposicoes"><span style="color:<?php echo $corT; ?>" class="text-right <?php if($caixa == 'sim'){?>text-uppercase<?php } ?> <?php echo $estiloTitle; ?>"><?php echo $titulo; ?></span> <span style="color:<?php echo $corS; ?>" class="text-right <?php echo $estiloSub; ?>"><?php echo $subtitulo; ?></span></h2>
						<a href="<?php echo $permalink; ?>" title="<?php echo $titulo; ?>"><img src="<?php echo $html[0]; ?>" alt="<?php echo $titulo; ?>" class="img-responsive margin-exposicao">
						</a>

				   </div>
            </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
</section>
  <?php get_footer(); ?>
  
